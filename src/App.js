import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import Header from './containers/Header';
import { getEpisodes } from './action-creators/episodes';
import Loader from './components/Loader';
import history from './_config/history'
import SiteMap from './_config/routes';
import './App.css';

const App = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getEpisodes())
  }, [dispatch]);

  return <BrowserRouter history={history}>
    <Header />
    <Loader />
    <SiteMap />
  </BrowserRouter>;
}

export default App;
