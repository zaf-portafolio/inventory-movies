import { LOADER_HIDE, LOADER_SHOW } from '../loader/constans';
import { EPISODES_SUCESS, EPISODES_ERROR } from './constants';
import { fetchEpisodes } from '../../services/episodes';

export const getEpisodes = (params = {}) => {

    return (dispatch) => {
        dispatch({ type: LOADER_SHOW })
        fetchEpisodes(params).then(e =>{
            console.log(e)
            dispatch({ type: EPISODES_SUCESS, payload: e})
            dispatch({ type: LOADER_HIDE })
        }).catch(e => {
            console.log(e)
            dispatch({ type: EPISODES_ERROR, payload: e})
            dispatch({ type: LOADER_HIDE })
        })
    };
}