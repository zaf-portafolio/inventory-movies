import React from 'react';
import { Switch, Route } from 'react-router-dom';
import routes from './routes';

const SiteMap = () => (
    <section className="section-container">
        <Switch>
            {routes.map(r => (
                <Route exact key={r.path} path={r.path} component={r.component} />
            ))}
        </Switch>
    </section>
)

export default SiteMap;
