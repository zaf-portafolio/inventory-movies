import * as R from './constants';
import Home from '../../containers/Home';
import FAQ from '../../containers/FAQ';

export default [
    {
        path: R.HOME,
        text: 'Inicio',
        icon: 'ion-ios-home',
        component: Home,
    },
    {
        path: R.FAQ,
        text: 'F.A.Q.',
        icon: 'ion-ios-home',
        component: FAQ,
    }
];