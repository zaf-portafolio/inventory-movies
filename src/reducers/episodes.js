import { EPISODES_ERROR, EPISODES_SUCESS} from '../action-creators/episodes/constants';

const initialState = {
    model: null,
    error: null,
}

const episodesReducer = (state = initialState, action) => {

    switch(action.type){
        case EPISODES_SUCESS:
            return { ...state, model: action.payload};
        case EPISODES_ERROR:
            return { ...state, error: action.error};
        default:
            return state;
    }

}

export default episodesReducer;
