import { combineReducers } from 'redux';
import episodes from './episodes';
import loader from './loader';

export default combineReducers({
    episodes,
    loader,
});