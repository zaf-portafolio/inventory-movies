import { LOADER_HIDE, LOADER_SHOW } from '../action-creators/loader/constans';

const initialState = {
    show: false
}

const loaderReducer = (state = initialState, action) => {

    switch(action.type){
        case LOADER_SHOW:
            return { show: true };
        case LOADER_HIDE:
            return { show: false };
        default:
            return state;
    }

}

export default loaderReducer;
