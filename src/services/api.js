import axios from 'axios';

const Api = () => {

    const baseURL = 'https://kitsu.io/';
    const instance = axios.create({
        baseURL,
    })

    return instance;

}

export default Api();