import api from './api'

export const fetchEpisodes = (params = {}) => api
    .get('/api/edge/anime/1555/episodes', params)
    .then(({ data }) => data)
    .catch(e => e)