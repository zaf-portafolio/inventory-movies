import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Nav from '../../_config/routes/routes'
import './Navigation.scss';


const Navigation = ({ background='#333333', hoverBackground, linkColor }) => {

    const [hoverIndex, setHoverIndex] = useState(-1);
    const [navOpen, setNavOpen] = useState(false)


    return (
        <nav
            className="responsive-toolbar"
        >
            <ul style={{ background }} className={navOpen ? 'active' : ''} >
                <figure onClick={() => setNavOpen(!navOpen)}>
                    <i className={'ion-ios-menu'} />
                </figure>
                {Nav.map((link, index) => {
                    return (
                        <li
                            key={`nav_${index}`}
                            onMouseEnter={() => setHoverIndex(index)}
                            onMouseLeave={() => setHoverIndex(-1)}
                            style={{ background: hoverIndex === index ? (hoverBackground || '#999') : '' }}
                        >
                            <Link to={link.path} style={{ color: linkColor }}>
                                {link.text}
                                <i className={link.icon} />
                            </Link>
                        </li>

                    )
                })}
            </ul>
        </nav>
    );
}

export default Navigation;