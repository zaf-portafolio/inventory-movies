import { get } from 'lodash';
import { createSelector } from 'reselect';

export const getEpisodes = (state) => get(state, 'episodes', {});

export const getItems = createSelector(
    [getEpisodes],
    (episodes) => get(episodes, 'items', []),
);

