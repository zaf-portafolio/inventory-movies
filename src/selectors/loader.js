import { get } from 'lodash';
import { createSelector } from 'reselect';

export const getLoader = (state) => get(state, 'loader', {});

export const getLoaderStatus = createSelector(
    [getLoader],
    (loader) => get(loader, 'show', false),
);

