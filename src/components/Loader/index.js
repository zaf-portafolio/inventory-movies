import React from 'react';
import { useSelector } from 'react-redux';
import { getLoaderStatus } from '../../selectors/loader';
import './style.scss';

const Loader = () => {
    const show = useSelector(getLoaderStatus);
    return show ? <div className="loader"/> : null ; 
}

export default Loader;