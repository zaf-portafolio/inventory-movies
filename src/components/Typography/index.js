import React from 'react';
import './Typography.scss';


const Typography = ({
    children,
    component,
    className,
    text,
}) => {
    const classStyle = `Typography ${className}`;
    let content = text;
    if(children) content = children;

    return React.createElement(`${component}`, {className: classStyle}, content);
}

export default Typography;
